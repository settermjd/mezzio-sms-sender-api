<?php
declare(strict_types=1);

namespace App\Service;

use App\ValueObjects\Twilio;
use Interop\Container\ContainerInterface;
use Mezzio\Exception\InvalidArgumentException;

class TwilioServiceFactory
{
    public function __invoke(ContainerInterface $container): TwilioService
    {
        $config = $container->has('config') ? $container->get('config') : [];

        if (is_null($config) || !array_key_exists('twilio', $config)) {
            throw new InvalidArgumentException('Twilio configuration not able to be retrieved.') ;
        }

        $twilioConfig = new Twilio(
            $config['twilio']['account_sid'],
            $config['twilio']['auth_token'],
            $config['twilio']['phone_number'],
        );
        return new TwilioService($twilioConfig);
    }
}