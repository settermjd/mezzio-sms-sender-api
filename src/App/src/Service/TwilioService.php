<?php

declare(strict_types=1);

namespace App\Service;

use App\ValueObjects\Twilio;
use Twilio\Exceptions\ConfigurationException;
use Twilio\Exceptions\TwilioException;
use Twilio\Rest\Api\V2010\Account\MessageInstance;
use Twilio\Rest\Client;
use Twilio\TwiML\MessagingResponse;

/**
 * Class TwilioService
 * @package App\Service
 */
class TwilioService
{
    private Twilio $configuration;
    private Client $client;

    /**
     * TwilioService constructor.
     * @param Twilio $configuration A Twilio object containing the Twilio configuration details
     * @throws ConfigurationException
     */
    public function __construct(Twilio $configuration)
    {
        $this->configuration = $configuration;
        $this->client = new Client(
            $this->configuration->getAccountSid(),
            $this->configuration->getAuthToken()
        );
    }

    /**
     * Send an SMS message
     * @param string $sendTo
     * @param string $body
     * @throws TwilioException
     */
    public function sendSMS(string $sendTo, string $body): MessageInstance
    {
        return $this->client->messages->create(
            $sendTo,
            [
                'from' => $this->configuration->getPhoneNumber(),
                'body' => $body
            ]
        );
    }

    /**
     * Uses Twilio's Lookup API to determine if a phone number is valid or not
     * @param string $sendTo
     * @param string $countryCode
     * @return bool
     */
    public function isValidPhoneNumber(string $sendTo, string $countryCode): bool
    {
        try {
            $phoneNumber = $this->client
                ->lookups
                ->v1
                ->phoneNumbers($sendTo)
                ->fetch([
                    'countryCode' => $countryCode
                ]);
        } catch (TwilioException $e) {
            return false;
        }

        return true;
    }

    public function replyToSMS(string $replyMessage): MessagingResponse
    {
        $message = new MessagingResponse();
        $message->message($replyMessage);

        return $message;
    }
}