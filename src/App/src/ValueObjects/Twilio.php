<?php

declare(strict_types=1);

namespace App\ValueObjects;

/**
 * Class Twilio
 * @package App\ValueObjects
 */
class Twilio
{
    private string $accountSid;
    private string $authToken;
    private string $phoneNumber;

    /**
     * Twilio constructor.
     * @param string $accountSid
     * @param string $authToken
     * @param string $phoneNumber
     */
    public function __construct(string $accountSid, string $authToken, string $phoneNumber)
    {
        $this->accountSid = $accountSid;
        $this->authToken = $authToken;
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @return string
     */
    public function getAccountSid(): string
    {
        return $this->accountSid;
    }

    /**
     * @return string
     */
    public function getAuthToken(): string
    {
        return $this->authToken;
    }

    /**
     * @return string
     */
    public function getPhoneNumber(): string
    {
        return $this->phoneNumber;
    }
}