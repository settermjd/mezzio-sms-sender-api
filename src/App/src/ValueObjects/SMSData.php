<?php

declare(strict_types=1);

namespace App\ValueObjects;

use Laminas\Form\Annotation;

class SMSData
{
    /**
     * @Annotation\Filter({"name":"StringTrim"})
     * @Annotation\Attributes({"type":"text"})
     * @Annotation\Validator({
     *     "name":"Regex",
     *     "options": {
     *          "pattern": "/((\+[0-9]{2})?|0{1})[0-9]{11}/"
     *     }
     * })
     * @Annotation\Options({"label": "Send To"})
     * @Annotation\ErrorMessage("The receiving phone number has to be in one of the following three forms: 0410 1234 5678, +61 410 1234 5678,  0061 410 1234 5678. Spaces are optional.")
     */
    public string $sendTo;

    /**
     * @Annotation\Filter({"name":"StringTrim"})
     * @Annotation\Validator({"name":"StringLength", "options":{"max":500}})
     * @Annotation\Attributes({"type":"tel"})
     * @Annotation\Options({"label": "Country Code"})
     */
    public string $countryCode;

    /**
     * @Annotation\Filter({"name":"StringTrim"})
     * @Annotation\Validator({"name":"StringLength", "options":{"max":500}})
     * @Annotation\Attributes({"type":"textarea"})
     * @Annotation\Options({"label": "Send To"})
     * @Annotation\ErrorMessage("The message body can be no longer than 500 characters.")
     */
    public string $body;
}