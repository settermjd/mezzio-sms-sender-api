<?php

declare(strict_types=1);

namespace App\Handler;

use App\Service\TwilioService;
use App\ValueObjects\SMSData;
use Laminas\Diactoros\Response\EmptyResponse;
use Laminas\Diactoros\Response\JsonResponse;
use Laminas\Form\Annotation\AnnotationBuilder;
use Laminas\Form\FormInterface;
use Mezzio\Router;
use Mezzio\Template\TemplateRendererInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class HomePageHandler implements RequestHandlerInterface
{
    /** @var Router\RouterInterface */
    private Router\RouterInterface $router;

    /** @var null|TemplateRendererInterface */
    private ?TemplateRendererInterface $template;

    private TwilioService $twilioService;
    private FormInterface $form;

    public function __construct(
        TwilioService $twilioService,
        Router\RouterInterface $router,
        ?TemplateRendererInterface $template = null
    ) {
        $this->router        = $router;
        $this->template      = $template;
        $this->twilioService = $twilioService;
        $this->form = (new AnnotationBuilder())->createForm(SMSData::class);
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        if ($request->getMethod() !== 'POST') {
            return new EmptyResponse(405);
        }

        $this->form->setData($request->getParsedBody());

        if ($this->form->isValid() &&
            $this->twilioService->isValidPhoneNumber(
                $this->form->get('sendTo')->getValue(),
                $this->form->get('countryCode')->getValue()
            )
        ) {
            $response = $this
                ->twilioService
                ->sendSMS(
                    $this->form->get('sendTo')->getValue(),
                    $this->form->get('body')->getValue()
                );

            return new JsonResponse([
                'from' => $response->from,
                'to' => $response->to,
                'body' => $response->body,
                'status' => $response->status
            ]);
        } else {
            return new JsonResponse(
                [
                    'status' => 'unsuccessful',
                    'reason' => $this->form->getMessages()
                ],
                403
            );
        }
    }
}
