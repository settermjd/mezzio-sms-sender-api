<?php

declare(strict_types=1);

namespace App\Handler;

use App\Service\TwilioService;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Psr\Container\ContainerInterface;
use Psr\Http\Server\RequestHandlerInterface;

class SMSReplyHandlerFactory
{
    public function __invoke(ContainerInterface $container): RequestHandlerInterface
    {
        if (!$container->has(TwilioService::class)) {
            throw new ServiceNotFoundException('Twilio service not found.');
        }
        $twilioService = $container->get(TwilioService::class);

        return new SMSReplyHandler($twilioService);
    }
}
