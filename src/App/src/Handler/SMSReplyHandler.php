<?php

declare(strict_types=1);

namespace App\Handler;

use App\Service\TwilioService;
use Laminas\Diactoros\Response\XmlResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class SMSReplyHandler implements RequestHandlerInterface
{
    const REPLY_MESSAGE = "Thanks for sending your SMS. We'll be in touch with you shortly.";
    private TwilioService $twilioService;

    public function __construct(TwilioService $twilioService)
    {
        $this->twilioService = $twilioService;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $messageResponse = $this->twilioService->replyToSMS(self::REPLY_MESSAGE);
        return new XmlResponse($messageResponse->__toString());
    }
}
